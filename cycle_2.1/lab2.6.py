def positive_list(list1):
    return[i for i in list1 if i>0]
def square_list(list1):
    return[i**2 for i in list1]
def vowel_list(word):
    return[i for i in word if i in "aeiouAEIOU"]
def ordinal_list(word):
    return[ord(i) for i in word]
print("Enter a list of no.s to generate the positive list: ")
list1=[int(i) for i in input().split()]
print("Positive list: ",positive_list(list1))
print("Enter a list of no.s to generate the square list: ")
list1=[int(i) for i in input().split()]
print("Square list: ",square_list(list1))
word=input("Enter a word to generate vowel list: ")
print("Vowel list: ",vowel_list(word))
word=input("Enter a word to generate ordinal value: ")
print("Ordinal value list: ",ordinal_list(word))
